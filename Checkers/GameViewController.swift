//
//  GameViewController.swift
//  Checkers
//
//  Created by Skip Wilson on 9/3/14.
//  Copyright (c) 2014 Skip Wilson. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit

class GameViewController: UIViewController {

    var moveSpace:Float = 0.24896006286144248
    var otherSide = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create a new scene
        let scene = SCNScene(named: "art.scnassets/checkerboard.dae")
        
        // retrieve the SCNView
        let scnView = self.view as SCNView
        
        // set the scene to the view
        scnView.scene = scene
        
        // allows the user to manipulate the camera
        scnView.allowsCameraControl = true
        
        // show statistics such as fps and timing information
        scnView.showsStatistics = true
        
        // configure the view
        scnView.backgroundColor = UIColor.grayColor()
        
        // add a tap gesture recognizer
        let gestureRecognizers = NSMutableArray()
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: "handleSwipeLeft:")
        swipeLeftGesture.direction = .Left
        
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: "handleSwipeRight:")
        swipeRightGesture.direction = .Right
        
        
        gestureRecognizers.addObject(swipeLeftGesture)
        gestureRecognizers.addObject(swipeRightGesture)

        
        scnView.gestureRecognizers = gestureRecognizers
    }
    
    func handleSwipeLeft(gestureRecognizer:UIGestureRecognizer) {
        movePiece(gestureRecognizer: gestureRecognizer, inDirection: .Left)
    }
    
    func handleSwipeRight(gestureRecognizer:UIGestureRecognizer) {
        movePiece(gestureRecognizer: gestureRecognizer, inDirection: .Right)
    }
    
    func movePiece(#gestureRecognizer:UIGestureRecognizer,inDirection:UISwipeGestureRecognizerDirection) {
        // retrieve the SCNView
        let scnView = self.view as SCNView
        
        // check what nodes are tapped
        let p = gestureRecognizer.locationInView(scnView)
        if let hitResults = scnView.hitTest(p, options: nil) {
            // check that we clicked on at least one object
            if hitResults.count > 0 {
                // retrieved the first clicked object
                let result: AnyObject! = hitResults[0]
                
                var nextSpot:SCNVector3 = SCNVector3(x: 0, y: 0, z: 0)
                
                if !result.node!.name!.hasPrefix("Circle") {
                    return
                }
                
                if inDirection == .Left {
                    if otherSide {
                        nextSpot.x = result.node!.position.x - moveSpace
                        nextSpot.z = result.node!.position.z + moveSpace
                        nextSpot.y = result.node!.position.y
                    } else {
                        nextSpot.x = result.node!.position.x + moveSpace
                        nextSpot.z = result.node!.position.z - moveSpace
                        nextSpot.y = result.node!.position.y
                    }
                } else {
                    if otherSide {
                        nextSpot.x = result.node!.position.x - moveSpace
                        nextSpot.z = result.node!.position.z - moveSpace
                        nextSpot.y = result.node!.position.y
                    } else {
                        nextSpot.x = result.node!.position.x + moveSpace
                        nextSpot.z = result.node!.position.z + moveSpace
                        nextSpot.y = result.node!.position.y
                    }
                }
                
                SCNTransaction.begin()
                SCNTransaction.setAnimationDuration(0.5)
                
                result.node!.position = nextSpot
                
                SCNTransaction.setCompletionBlock {
                    self.nextMove()
                }
                
                SCNTransaction.commit()
            }
        }
    }
    
    func nextMove() {
        SCNTransaction.begin()
        SCNTransaction.setAnimationDuration(2)
        let scnView = self.view as SCNView
        
        var plane = scnView.scene!.rootNode.childNodeWithName("Plane", recursively: true)
        var base = scnView.scene!.rootNode.childNodeWithName("Base", recursively: true)
        
        var rotateTo = otherSide ? Float(0) : Float(M_PI)
        plane!.rotation = SCNVector4(x: 0, y: 1, z: 0, w: rotateTo)
        base!.rotation = SCNVector4(x: 0, y: 1, z: 0, w: rotateTo)
        SCNTransaction.commit()
        
        if !otherSide {
            otherSide = true
        } else {
            otherSide = false
        }
    }
    
    func handleTap(gestureRecognize: UIGestureRecognizer) {
        // retrieve the SCNView
        let scnView = self.view as SCNView
        
        // check what nodes are tapped
        let p = gestureRecognize.locationInView(scnView)
        if let hitResults = scnView.hitTest(p, options: nil) {
            // check that we clicked on at least one object
            if hitResults.count > 0 {
                // retrieved the first clicked object
                let result: AnyObject! = hitResults[0]
                
                println(result.node!.position.x)
                println(result.node!.position.y)
                println(result.node!.position.z)
            }
        }
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.toRaw())
        } else {
            return Int(UIInterfaceOrientationMask.All.toRaw())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

}
